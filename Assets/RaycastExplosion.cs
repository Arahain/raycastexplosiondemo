﻿using System;

using UnityEngine;

public class RaycastExplosion : MonoBehaviour {
    public float radius = 5;
    public float force = 200;
    void Start() {

    }

    void Update() {
        if(Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 10000)) { // you can also filter for specific layers in Physics.Raycast!
                Vector3 clickPosition = hit.point;
                Debug.Log(clickPosition);

                RaycastHit[] spherecastHits = Physics.SphereCastAll(clickPosition, radius, Vector3.forward);
                foreach (RaycastHit spherecastHit in spherecastHits) {
                    try {
                        Vector3 direction = spherecastHit.transform.position - clickPosition;
                        direction = direction.normalized + Vector3.up;
                        spherecastHit.transform.gameObject.GetComponent<Rigidbody>().AddForce(direction.normalized * force, ForceMode.Impulse);
                    } catch (Exception e) {

                    }
                }
            }
        }
    }


}
